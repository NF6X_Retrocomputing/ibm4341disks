#IBM 4341 and 3880 Disk Images

These images were created with [ImageDisk 1.18](http://www.classiccmp.org/dunfield/img/index.htm) from a stack of 8" floppy disks found in a military surplus communications shelter. ImageDisk reported errors when reading the two 3880 maintenance device disks. There were two identical copies of the FUNC disk. The contents of the unlabeled MISC disks are unknown. MISC002.IMD is nearly identical to MISC003.IMD, and MISC003.IMD represents three identical disks.


![](3880_small.jpg?raw=true)
![](3880label_small.jpg?raw=true)
![](DIAG_small.jpg?raw=true)
![](FUNC_small.jpg?raw=true)
![](MISC_small.jpg?raw=true)
![](BOM1_small.jpg?raw=true)
![](BOM2_small.jpg?raw=true)
